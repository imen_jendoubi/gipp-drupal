<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/we_megamenu-8.x-1.11/we_megamenu/templates/we-megamenu-frontend.html.twig */
class __TwigTemplate_72c1f4f9ff519ec4af99be85ded3e6e30fabc3e23496e9ec58e223e9f698e0f5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"region-we-mega-menu\">
\t<a class=\"navbar-toggle collapsed\">
\t    <span class=\"icon-bar\"></span>
\t    <span class=\"icon-bar\"></span>
\t    <span class=\"icon-bar\"></span>
\t</a>
\t<nav ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
\t  <div class=\"container-fluid\">
\t    ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
\t  </div>
\t</nav>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/we_megamenu-8.x-1.11/we_megamenu/templates/we-megamenu-frontend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 9,  63 => 7,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"region-we-mega-menu\">
\t<a class=\"navbar-toggle collapsed\">
\t    <span class=\"icon-bar\"></span>
\t    <span class=\"icon-bar\"></span>
\t    <span class=\"icon-bar\"></span>
\t</a>
\t<nav {{ attributes }}>
\t  <div class=\"container-fluid\">
\t    {{ content }}
\t  </div>
\t</nav>
</div>", "modules/we_megamenu-8.x-1.11/we_megamenu/templates/we-megamenu-frontend.html.twig", "C:\\xampp\\htdocs\\gipp-drupal\\modules\\we_megamenu-8.x-1.11\\we_megamenu\\templates\\we-megamenu-frontend.html.twig");
    }
}
